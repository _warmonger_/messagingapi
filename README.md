Finished, not tested

MessagingAPI is a kind of library, which actually allows for communication between servers in Bungeecord network.

Bungeecord is a proxy server for Minecraft Server networks. It acts as a bridge between player and destination server and it makes it easier to create and maintain a network of multiple server instances.

There is a mechanism for server-server communication, but it is not dedicated and created using vanilla server features not intended for that: packets designed for communication between player and server.
This solution is working, but there is at least one player online required on both of the servers for the data exchange to be proceeded, which is, in my opinion, huge disadvantage, and thats why MessagingAPI was created.

MessagingAPI consists of three projects:
 - MessagingServer - Plugin for Minecraft Server, which enables MessagingAPI communication feature on a particular server;
 - MessagingProxy - Plugin for Bungeecord Server, which does the same thing on Bungeecord proxy server;
 - MessagingAPI - consists of code used commonly by MessagingServer and MessagingProxy.
 
 Currently, protocol of communication involves data encryption (RSA/AES).
 
 MessagingAPI common code was so designed, that it does not use any of Minecraft/Bungeecord server code; it is independent code (despite usage of utility code delivered by Google), deliberately developed in
 that way, so it may be used by any other project or network.
 
 Email: michal.s.jagodzinski@gmail.com