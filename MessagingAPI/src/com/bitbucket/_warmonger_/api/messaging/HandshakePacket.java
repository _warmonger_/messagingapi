package com.bitbucket._warmonger_.api.messaging;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;


/** Provides support for handshake sending/receiving
 * @author An0nym8us
 *
 */
public class HandshakePacket
{
	public SecretKey key;
	public byte[] IV;
	public String serverName;
	public String password;
	public int packetID = 0x00;
	
	public HandshakePacket(SecretKey key, byte[] IV, String serverName, String password)
	{
		this.key = key;
		this.IV = IV;
		this.serverName = serverName;
		this.password = password;
	}
	
	public HandshakePacket(InputStream input) throws IOException
	{
		DataInputStream dis = new DataInputStream(input);	
		
		int length = dis.readInt();
		
		byte[] data = new byte[length];	
		dis.readFully(data);
		
		dis = new DataInputStream(new ByteArrayInputStream(data));
		int packetID = dis.readInt();
		int secretLength = dis.readInt();
		int ivLength = dis.readInt();
		
		byte[] key = new byte[secretLength];
		this.IV = new byte[ivLength];
		
		dis.read(key, 0, key.length);
		dis.read(IV, 0, IV.length);
		
		this.key = new SecretKeySpec(key, 0, key.length, "AES");
		
		this.serverName = dis.readUTF();
		
		byte[] decryptedPassword = new byte[dis.readInt()];
		dis.read(decryptedPassword);
		
		try
		{
			decryptedPassword = CryptoStance.Decrypt(this.key, IV, decryptedPassword);
			
			this.password = new String(decryptedPassword, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean WritePacket(OutputStream output)
	{
		try
		{
			ByteArrayDataOutput outStream = ByteStreams.newDataOutput();			
			outStream.writeInt(packetID);
			outStream.writeInt(key.getEncoded().length);
			outStream.writeInt(IV.length);
			outStream.write(key.getEncoded());
			outStream.write(IV);
			outStream.writeUTF(serverName);
			
			byte[] encryptedPassword = CryptoStance.Encrypt(key, IV, password.getBytes("UTF-8"));
			
			outStream.writeInt(encryptedPassword.length);
			outStream.write(encryptedPassword);
			
			DataOutputStream dos = new DataOutputStream(output);
			dos.writeInt(outStream.toByteArray().length);			
			dos.write(outStream.toByteArray());
		}
		catch(Exception ex)
		{
			return false;
		}
		
		return true;
	}
}
