package com.bitbucket._warmonger_.api.messaging.server.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;

/** Called when server has connected to proxy
 * @author An0nym8us
 *
 */
public class ServerConnectedEvent implements MessagingEvent
{
	public String serverName;
	
	public ServerConnectedEvent(String serverName)
	{
		this.serverName = serverName;
	}
}
