package com.bitbucket._warmonger_.api.messaging.server.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;

/** Called when server has attemted to connect to proxy with invalid password given
 * @author An0nym8us
 *
 */
public class InvalidPasswordConnectionTrialEvent implements MessagingEvent
{
	public String serverName;
	public String passwordGiven;
	
	public InvalidPasswordConnectionTrialEvent(String serverName, String passwordGiven)
	{
		this.serverName = serverName;
		this.passwordGiven = passwordGiven;
	}
}
