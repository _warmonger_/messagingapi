package com.bitbucket._warmonger_.api.messaging.server.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;

/** Called when server has disconnected from proxy
 * @author An0nym8us
 *
 */
public class ServerDisconnectedEvent implements MessagingEvent
{
	public String serverName;
	
	public ServerDisconnectedEvent(String serverName)
	{
		this.serverName = serverName;
	}
}
