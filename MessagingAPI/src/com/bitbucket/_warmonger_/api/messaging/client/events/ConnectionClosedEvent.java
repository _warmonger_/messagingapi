package com.bitbucket._warmonger_.api.messaging.client.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;



/** Called when connection is closed by proxy - caused by reason other than connection loss (i.e. connection duplex)
 * @author An0nym8us
 *
 */
public class ConnectionClosedEvent implements MessagingEvent
{
	public enum DisconnectReason
	{
		DuplicatedConnection(),
		InvalidPassword;
		
		DisconnectReason() { }
	}
	public DisconnectReason reason;
	
	public ConnectionClosedEvent(DisconnectReason reason)
	{
		this.reason = reason;
	}
}
