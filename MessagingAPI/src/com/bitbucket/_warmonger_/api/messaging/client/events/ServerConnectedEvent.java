package com.bitbucket._warmonger_.api.messaging.client.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;

/** Called when server has connected to proxy
 * @author An0nym8us
 *
 */
public class ServerConnectedEvent implements MessagingEvent
{
	public ServerConnectedEvent()
	{
		
	}
}
