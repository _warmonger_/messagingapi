package com.bitbucket._warmonger_.api.messaging.client.events;

import com.bitbucket._warmonger_.api.events.MessagingEvent;

/** Called when proxy disconnected by connection loss
 * @author An0nym8us
 *
 */
public class ProxyDisconnectedEvent implements MessagingEvent
{	
	public ProxyDisconnectedEvent()
	{
		
	}
}
