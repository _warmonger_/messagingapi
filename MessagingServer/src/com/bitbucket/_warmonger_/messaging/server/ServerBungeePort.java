package com.bitbucket._warmonger_.messaging.server;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bukkit.plugin.java.JavaPlugin;

import com.bitbucket._warmonger_.api.events.EventHandler;
import com.bitbucket._warmonger_.api.events.EventManager;
import com.bitbucket._warmonger_.api.events.MessagingListener;
import com.bitbucket._warmonger_.api.messaging.client.ServerListener;
import com.bitbucket._warmonger_.api.messaging.client.events.MessageReceivedEvent;
import com.bitbucket._warmonger_.api.messaging.server.ProxyListener;
import com.google.common.base.Joiner;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class ServerBungeePort implements MessagingListener
{
	public static String CHANNEL = "BungeeCord";
	static ServerBungeePort instance;
	boolean isReceived;
	String data;
	
	public enum SubChannel
	{
		Connect("Connect"),
		IP("IP"),
		PlayerCount("PlayerCount"),
		PlayerList("PlayerList"),
		GetServers("GetServers"),
		Message("Message"),
		GetServer("GetServer"),
		UUID("UUID"),
		ServerIP("ServerIP"),
		KickPlayer("KickPlayer");
		
		public String name;
		
		SubChannel(String name)
		{
			this.name = name;
		}
		
		public static boolean IsCustom(String subChannel)
		{
			return !Arrays.asList(SubChannel.GetNames()).contains(subChannel);
		}
		
		static String[] GetNames()
		{
			String[] result = new String[SubChannel.values().length];
			
			for(int i = 0; i < SubChannel.values().length; i++)
			{
				result[i] = SubChannel.values()[i].name;
			}
			
			return result;
		}
	}
	
	List<PacketWaiter> waiters;
	int waiterCounter = 0;
	
	public ServerBungeePort()
	{
		instance = this;
		waiters = new ArrayList<PacketWaiter>();
		
		EventManager.registerListener(this);
	}
	
	public static ServerBungeePort getInstance()
	{
		return instance;
	}
	
	public void SendRequest(SubChannel subChannel, String... data)
	{
		List<String> list = new ArrayList<String>(Arrays.asList(data));
		list.add(0, subChannel.name);
		data = list.toArray(new String[list.size()]);
		
		try
		{
			ServerListener.getInstance().SendMessage(CHANNEL, ServerListener.SERVER_PROXY, data);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public String[] SendPreparedRequest(final SubChannel subChannel, final String... data) throws InterruptedException, ExecutionException, TimeoutException
	{
		Future<String[]> f = waitForReceivedData(subChannel, data);		
		
		return f.get(1000, TimeUnit.MILLISECONDS);
	}
	
	Future<String[]> waitForReceivedData(final SubChannel subChannel, final String... data)
	{
		return Executors.newFixedThreadPool(1).submit(new Callable<String[]>()
				{
					public String[] call() throws InterruptedException, IOException
					{
						PacketWaiter pw;
						
						List<String> l = new ArrayList<String>(Arrays.asList(data));
						l.add(0, subChannel.name);
						String[] data = l.toArray(new String[l.size()]);
							
						pw = new PacketWaiter(waiterCounter++, data);
						waiters.add(pw);
							
						synchronized(pw)
						{	
							ServerListener.getInstance().SendMessage(CHANNEL, ServerListener.SERVER_PROXY, pw.GetSentParams());
							if(!pw.IsDone())
							{
								pw.wait();
							}
						}
						
						String[] res = pw.GetReceivedParams();
						waiters.remove(pw);
						
						return res;
					}
				});
	}
	
	@EventHandler
	public void onMessageReceived(MessageReceivedEvent ev)// throws IOException
	{
		if(ev.channel.equals(CHANNEL) && !SubChannel.IsCustom(ev.params[1]))
		{
			for(PacketWaiter pw : waiters)
			{
				if(pw.ID == PacketWaiter.GetID(ev.params))
				{
					if(!pw.IsDone())						
					{
						synchronized(pw)
						{
							pw.SetDone(ev.params);
							pw.notify();
						}
					}
					
					return;
				}
			}
		}
	}
}
