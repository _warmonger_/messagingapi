package com.bitbucket._warmonger_.messaging.server;

import org.bukkit.Bukkit;

import com.bitbucket._warmonger_.api.events.EventHandler;
import com.bitbucket._warmonger_.api.events.MessagingListener;
import com.bitbucket._warmonger_.api.messaging.client.events.ConnectionClosedEvent;
import com.bitbucket._warmonger_.api.messaging.client.events.MessageReceivedEvent;
import com.bitbucket._warmonger_.api.messaging.client.events.MessageSentEvent;
import com.bitbucket._warmonger_.api.messaging.client.events.ProxyDisconnectedEvent;
import com.bitbucket._warmonger_.api.messaging.client.events.ServerConnectedEvent;


public class EventListener implements MessagingListener
{
	@EventHandler
	public void onServerConnected(ServerConnectedEvent ev)
	{
		Bukkit.getServer().getLogger().info("Server connected to proxy!");
	}
	
	@EventHandler
	public void onProxyDisconnected(ProxyDisconnectedEvent ev)
	{
		Bukkit.getServer().getLogger().info("Server disconnected from proxy!");
	}
	
	@EventHandler
	public void onConnectionClosed(ConnectionClosedEvent ev)
	{
		Bukkit.getServer().getLogger().info("Connection issue, aborted; Reason: " + ev.reason.name());
	}
}
