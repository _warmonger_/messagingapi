package com.bitbucket._warmonger_.messaging.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PacketWaiter
{
	Object latch;
	public int ID;
	public String[] params;
	boolean _done;
	
	public PacketWaiter(int ID, String[] params)
	{
		this.ID = ID;
		
		this.params = params;
	}
	
	public boolean IsDone()
	{
		return _done;
	}
	
	public String[] GetSentParams()
	{
		List<String> l = new ArrayList<String>(Arrays.asList(params));
		l.add(0, Integer.toString(ID));
		
		return l.toArray(new String[l.size()]);
	}
	
	public void SetSentParams(String[] params)
	{
		this.params = params;
	}
	
	public String[] GetReceivedParams()
	{
		return params;
	}
	
	public void SetReceivedParams(String[] params)
	{
		List<String> l = new ArrayList<String>(Arrays.asList(params));
		l.remove(0);
		
		this.params = l.toArray(new String[l.size()]);
		
		_done = true;
	}
	
	public void SetDone(String[] params)
	{
		SetReceivedParams(params);
	}
	
	public static int GetID(String[] params)
	{
		return Integer.parseInt(params[0]);
	}
}