package com.bitbucket._warmonger_.messaging.server;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.monstercraft.info.plugin.utils.JarUtils;

import com.bitbucket._warmonger_.api.events.EventManager;
import com.bitbucket._warmonger_.api.messaging.client.ServerListener;
import com.bitbucket._warmonger_.api.messaging.server.ProxyListener;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

public class Main extends JavaPlugin
{
	String serverName;
	String proxyServer;
	int proxyPort;
	
	EventListener listener;
	
	ServerListener serverListener;
	
	public void onEnable()
	{
		listener = new EventListener();
		EventManager.registerListener(listener);
		
		serverName = null;
		
		loadConfiguration();
		
		try
		{
			this.serverName = getConfig().getString("serverName");
			this.proxyServer = getConfig().getString("proxyIP");
			this.proxyPort = getConfig().getInt("proxyPort");
			
			serverListener = new ServerListener(serverName, proxyServer, proxyPort, getConfig().getString("proxyPassword"));
			
			new ServerBungeePort();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			Bukkit.getServer().getLogger().info("Plugin crashed. Disabling...");
			
			Bukkit.getServer().getPluginManager().disablePlugin(this);
		}
		
		Bukkit.getServer().getLogger().info("MessagingServer enabled!");
	}
	
	public void onDisable()
	{
		serverListener.Dispose();
		
		EventManager.unregisterAllListeners();
		
		Bukkit.getServer().getLogger().info("MessagingServer disabled!");
	}
	
	public void loadConfiguration()
	{
		getConfig().addDefault("serverName", "testServer");
		getConfig().addDefault("proxyIP", "127.0.0.1");
		getConfig().addDefault("proxyPort", 30001);
		getConfig().addDefault("proxyPassword", "");

		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args)
	{
		if(command.getName().equals("sendp"))
		{
			
			try {
				ServerListener.getInstance().SendMessage(args[0], args[1], Arrays.copyOfRange(args, 2, args.length));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				sender.sendMessage("Packet sent!");
			}
		}
		return false;
	}
}
