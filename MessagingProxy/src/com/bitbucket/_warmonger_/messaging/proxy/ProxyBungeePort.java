package com.bitbucket._warmonger_.messaging.proxy;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Arrays;

import com.bitbucket._warmonger_.api.events.EventHandler;
import com.bitbucket._warmonger_.api.events.EventManager;
import com.bitbucket._warmonger_.api.events.MessagingListener;
import com.bitbucket._warmonger_.api.messaging.server.ProxyListener;
import com.bitbucket._warmonger_.api.messaging.server.events.MessageReceivedEvent;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;

public class ProxyBungeePort implements MessagingListener
{
	public static String CHANNEL = "BungeeCord";
	static ProxyBungeePort instance;
	
	enum SubChannel
	{
		Connect("Connect"),
		IP("IP"),
		PlayerCount("PlayerCount"),
		PlayerList("PlayerList"),
		GetServers("GetServers"),
		Message("Message"),
		GetServer("GetServer"),
		UUID("UUID"),
		ServerIP("ServerIP"),
		KickPlayer("KickPlayer");
		
		public String name;
		
		SubChannel(String name)
		{
			this.name = name;
		}
		
		public static boolean IsCustom(String subChannel)
		{
			return !Arrays.asList(SubChannel.GetNames()).contains(subChannel);
		}
		
		static String[] GetNames()
		{
			String[] result = new String[SubChannel.values().length];
			
			for(int i = 0; i < SubChannel.values().length; i++)
			{
				result[i] = SubChannel.values()[i].name;
			}
			
			return result;
		}
		
		public static SubChannel GetByName(String name)
		{
			for(SubChannel sc : SubChannel.values())
			{
				if(sc.name.equals(name))
				{
					return sc;
				}
			}
			
			return null;
		}
	}
	
	public ProxyBungeePort()
	{
		instance = this;
		
		EventManager.registerListener(this);
	}
	
	public static ProxyBungeePort GetInstance()
	{
		return instance;
	}
	
	@EventHandler
	public void onMessageReceived(MessageReceivedEvent ev) throws IOException
	{
		if(ev.channel.equals(CHANNEL))
		{
			ByteArrayInputStream bis = new ByteArrayInputStream(ev.GetData());
			DataInputStream dis = new DataInputStream(bis);
			String subChannel = dis.readUTF();
			
			ByteArrayDataOutput outStream = ByteStreams.newDataOutput();
			
			outStream.writeUTF(subChannel);
			
			switch(SubChannel.GetByName(subChannel))
			{
			case Connect:
				String serverTo = dis.readUTF();
				String playerName = dis.readUTF();
				
				BungeeCord.getInstance().getPlayer(playerName).connect(BungeeCord.getInstance().getServerInfo(serverTo));
				break;
			case IP:
				playerName = dis.readUTF();
				
				outStream.writeUTF(BungeeCord.getInstance().getPlayer(playerName).getAddress().getHostString());
				outStream.writeInt(BungeeCord.getInstance().getPlayer(playerName).getAddress().getPort());
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case PlayerCount:
				String serverName = dis.readUTF();
				
				if(serverName.equals("ALL"))
				{
					outStream.writeInt(BungeeCord.getInstance().getOnlineCount());
				}
				else
				{
					outStream.writeInt(BungeeCord.getInstance().getServerInfo(serverName).getPlayers().size());
				}
				
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case GetServer:
				playerName = "SirCaliburn";
				outStream.writeUTF(BungeeCord.getInstance().getPlayer(playerName).getServer().getInfo().getName());
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case PlayerList:
				serverName = dis.readUTF();
				
				if (serverName.equals("ALL"))
                {
					outStream.writeUTF( Util.csv( BungeeCord.getInstance().getPlayers() ) );
                }
				else
                {
                    ServerInfo server = BungeeCord.getInstance().getServerInfo(serverName);
                    if (server != null)
                    {
                        outStream.writeUTF(Util.csv(server.getPlayers()));
                    }
                }
				
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case GetServers:				
				outStream.writeUTF(Util.csv( BungeeCord.getInstance().getServers().keySet() ));
				
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case Message:
				BungeeCord.getInstance().getPlayer(dis.readUTF()).sendMessage(new TextComponent(dis.readUTF()));
				break;
			case UUID:
				playerName = dis.readUTF();
				outStream.writeUTF(BungeeCord.getInstance().getPlayer(playerName).getUniqueId().toString());
				
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case ServerIP:
				serverName = dis.readUTF();
				
				outStream.writeUTF(BungeeCord.getInstance().getServerInfo(serverName).getAddress().getHostString());
				outStream.writeInt(BungeeCord.getInstance().getServerInfo(serverName).getAddress().getPort());
				
				ProxyListener.GetInstance().SendMessage(CHANNEL, ev.serverFrom, outStream.toByteArray());
				break;
			case KickPlayer:
				playerName = dis.readUTF();
				
				BungeeCord.getInstance().getPlayer(playerName).disconnect(new TextComponent(dis.readUTF()));
				break;
			}
		}
	}
}
