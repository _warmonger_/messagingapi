package com.bitbucket._warmonger_.messaging.proxy;

import java.io.File;

import com.bitbucket._warmonger_.api.events.EventManager;
import com.bitbucket._warmonger_.api.messaging.server.ProxyListener;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Main extends Plugin
{
	private static boolean DEBUG_INFO = false;

	Configuration config;

	ProxyListener proxyListener;
	ProxyBungeePort proxyBungeePort;

	public void onEnable()
	{
		EventManager.registerListener(new EventListener());

		LoadConfig();
		
		try
		{
			proxyListener = new ProxyListener(config.getInt("port"), config.getBoolean("passwordEnabled") ? null : config.getString("password"));
			proxyBungeePort = new ProxyBungeePort();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}

	private boolean LoadConfig()
	{
		try
		{
			if (!getDataFolder().exists())
			{
				getDataFolder().mkdirs();
			}
			
			File file = new File(getDataFolder(), "config.yml");
			
			if (!file.exists())
			{
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
				
				
				config.set("port", ProxyListener.DEFAULT_PORT);
				config.set("passwordEnabled", true);
				config.set("password", "1234");
			}
			else
			{
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();

			return false;
		}
		
		return true;
	}
}
