package com.bitbucket._warmonger_.messaging.proxy;

import com.bitbucket._warmonger_.api.events.EventHandler;
import com.bitbucket._warmonger_.api.events.MessagingListener;
import com.bitbucket._warmonger_.api.messaging.server.events.DataReceivingCrashedEvent;
import com.bitbucket._warmonger_.api.messaging.server.events.DuplicatedServerConnectionTrialEvent;
import com.bitbucket._warmonger_.api.messaging.server.events.ServerConnectedEvent;
import com.bitbucket._warmonger_.api.messaging.server.events.ServerDisconnectedEvent;

import net.md_5.bungee.BungeeCord;

public class EventListener implements MessagingListener
{
	@EventHandler
	public void onServerConnected(ServerConnectedEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Proxy connected with " + ev.serverName);
	}
	
	@EventHandler
	public void onServerDisconnected(ServerDisconnectedEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Server disconnected: " + ev.serverName);
	}
	
	@EventHandler
	public void onServerDuplicatedConnection(DuplicatedServerConnectionTrialEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Server duplicated connection trial: " + ev.serverName);
	}
	
	/*@EventHandler
	public void onMessageSent(MessageSentEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Message sent!");
	}
	
	@EventHandler
	public void onMessageReceived(MessageReceivedEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Message received!");
	}
	
	@EventHandler
	public void onMessageReceiving(MessageReceivingEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("Message is receiving!");
	}*/
	
	@EventHandler
	public void onDataReceivingCrashed(DataReceivingCrashedEvent ev)
	{
		BungeeCord.getInstance().getLogger().info("RECEIVING CRASHED!");
	}
}
